function ola(){$("#ship-number,#ship-more-info").attr("maxlength",10),$("#ship-street").attr("maxlength","30"),$("#client-phone").attr("placeholder","+569XXXXXXXX"),$("#ship-number").attr("data-toggle","onlynumber"),$("#ship-number").attr("title","Por favor, solo números."),$('[data-toggle="onlynumber"]').tooltip(),$(".ship-reference.input.text").removeClass("hide"),$("#ship-street").attr("data-toggle","words"),$("#ship-street").attr("title","Por favor, solo letras.")}$(window).on("load",(function(){ola()})),$(document).ajaxStop((function(){ola(),$(".row-fluid.orderform-template").hasClass("active")&&!$(".terms-agree").length&&($("#client-profile-data .newsletter").after('<p class="terms-agree">Acepto <a \t\t\t \t\ttarget="_blank" href="/ayuda/terminos-condiciones">Términos y Condiciones</a> </p> \t<div id="overlayCheckoutButton"></div>'),$("#go-to-shipping").attr("disabled","disabled"),$("#opt-in-newsletter").is(":checked")?$(".newsletter").addClass("selected"):$(".newsletter").removeClass("selected"),$(".terms-agree").click((function(){$(this).toggleClass("selected"),$("#overlayCheckoutButton").toggleClass("disabled"),$(this).hasClass("selected")?$("#go-to-shipping").removeAttr("disabled"):$("#go-to-shipping").attr("disabled","disabled")})),$(".newsletter").click((function(){$("#opt-in-newsletter").is(":checked")?$(".newsletter").addClass("selected"):$(".newsletter").removeClass("selected")}))),$("#client-first-name").attr("maxlength",30),$("#client-last-name").attr("maxlength",30)}));var script=document.createElement("script");script.src="https://www.mercadopago.com/v2/security.js",script.setAttribute("output","vtex.deviceFingerprint"),script.setAttribute("view","checkout"),document.body.appendChild(script);
function changeAddres(){
    $('#ship-complement').live('change keyup paste', function(){
        if ($('#ship-complement').val().length > 7) {
            $('#ship-complement').val($('#ship-complement').val().substr(0,7));
        }
    });
    $('#ship-number').live('change keyup paste', function(){
        $('#ship-number').prop("type", "number")
        $('#ship-number').attr("onKeypress", "if (event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;")
        if ($('#ship-number').val().length > 10) {
            $('#ship-number').val($('#ship-number').val().substr(0,10));
        }
    });
    $('#ship-street').live('change keyup paste', function(){
        if ($('#ship-street').val().length > 30) {
            $('#ship-street').val($('#ship-street').val().substr(0,30));
        }
    });
}


$(window).on( "load", function() {
    
    changeAddres();
});