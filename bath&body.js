window.onhashchange = validarFormulario

function validarFormulario(){
    //VALIDAR NOMBRES
document.querySelector('#client-first-name').setAttribute('placeholder', "Ejemplo: Abel")
const limpiarNombre = () => {
    let limpiar = document.getElementById("client-first-name");
    limpiar.value = "";
};

document.querySelector('#client-last-name').setAttribute('placeholder', "Ejemplo: Perez Gonzalez")
const limpiarApellidos = () => {
    let limpiar = document.getElementById("client-last-name");
    limpiar.value = "";
};

//LIMPIAR SPAN
const limpiarSpanNombres = () => {
    let limpiar = document.getElementById("spanNombre");
    limpiar.innerHTML = "";
};
const limpiarSpanApellidos = () => {
    let limpiar = document.getElementById("spanApellidos");
    limpiar.innerHTML = "";
};

//Funciones
let nombreInput = document.getElementById("client-first-name")
const nombreInputSpan = document.getElementById("spanNombre")
nombreInput.addEventListener("blur", () => {
 let nombre = document.getElementById("client-first-name").value
    const expresionNombre = /[a-zA-ZäöüßÄÖÜ]/;
    if (nombre.match(expresionNombre)) {
        console.log(nombre);
        limpiarSpanNombres();
    } else {
        nombreInputSpan.textContent = "Ingrese sólo letras"
        limpiarNombre();
    }
});

let apellidoInput = document.getElementById("client-last-name")
const nombreInputApellidos = document.getElementById("spanApellidos")
apellidoInput.addEventListener("blur", () => {
 let apellidos = document.getElementById("client-last-name").value
    const expresionApellidos = /(\w.+\s).+/;
    if (apellidos.match(expresionApellidos)) {
        console.log(apellidos);
        limpiarSpanApellidos();
    } else {
        limpiarApellidos();
        nombreInputApellidos.textContent = "Ingrese 2 apellidos"
    }
});

//Agrega nombre de la región
$("#ship-state option[value='I Región']").append(' - Tarapaca');
$("#ship-state option[value='II Región']").append(' - Antofagasta');
$("#ship-state option[value='III Región']").append(' - Atacama');
$("#ship-state option[value='IV Región']").append(' - Coquimbo');
$("#ship-state option[value='V Región']").append(' - Valparaiso');
$("#ship-state option[value='VI Región']").append(" - O'Higgins");
$("#ship-state option[value='IX Región']").append(' - La Araucanía');
$("#ship-state option[value='VII Región']").append(' - Maule');
$("#ship-state option[value='VIII Región']").append(' - BíoBío');
$("#ship-state option[value='X Región']").append(' - Los Lagos');
$("#ship-state option[value='XI Región']").append(' - Aysén');
$("#ship-state option[value='XII Región']").append(' - Magallanes');
$("#ship-state option[value='XIV Región']").append(' - Los Ríos');
$("#ship-state option[value='XV Región']").append(' - Arica y Parinacota');
$("#ship-state option[value='XVI Región']").append(' - Ñuble');
  



//VALIDACIÓN DE LARGO DE LOS CAMPOS DE ENTREGA
 //Calle
 $('#ship-street').live('change keyup paste', function() {
    if ($('#ship-street').val().length > 25) {
        $('#ship-street').val($('#ship-street').val().substr(0, 25));
    }
});
//Numero
$('#ship-number').live('change keyup paste', function() {
    if ($('#ship-number').val().length > 6) {
        $('#ship-number').val($('#ship-number').val().substr(0, 6));
    }
});
//Piso o apartamento
$('#ship-complement').live('change keyup paste', function() {
    if ($('#ship-complement').val().length > 6) {
        $('#ship-complement').val($('#ship-complement').val().substr(0, 6));
    }
});
//Nombre de quién recibe
$('#ship-receiverName').live('change keyup paste', function() {
    if ($('#ship-receiverName').val().length > 13) {
        $('#ship-receiverName').val($('#ship-receiverName').val().substr(0, 13));
    }
});

}

document.body.onload = addElement;

function addElement () { 
    let spanNombre = document.createElement("span");
    spanNombre.setAttribute("id", "spanNombre")
   // spanNombre.innerHTML = "Ingrese sólo letras";
    document.getElementById("client-first-name").after(spanNombre);

    let spanApellidos = document.createElement("span");
    spanApellidos.setAttribute("id", "spanApellidos")
    //spanApellidos.innerHTML = "Ingrese 2 apellidos";
    document.getElementById("client-last-name").after(spanApellidos);
}