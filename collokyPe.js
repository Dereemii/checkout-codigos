/*truncando direccion*/
$('#ship-street').live('change keyup paste', function(){
    if ($('#ship-street').val().length > 30) {
        $('#ship-street').val($('#ship-street').val().substr(0,30));
    }
    });
    /*truncando numero*/
    $('#ship-number').live('change keyup paste', function(){
    if ($('#ship-number').val().length > 5) {
        $('#ship-number').val($('#ship-number').val().substr(0,5));
    }
    });
    
    /*truncando oficina*/
    $('#ship-more-info').live('change keyup paste', function(){
    if ($('#ship-more-info').val().length > 7) {
        $('#ship-more-info').val($('#ship-more-info').val().substr(0,7));
    }
    });
    
    /*truncando contacto*/
    $('#ship-name').live('change keyup paste', function(){
    if ($('#ship-name').val().length > 25) {
        $('#ship-name').val($('#ship-name').val().substr(0,25));
    }
    });
    
    /*truncando telefono*/
    $('#client-phone').live('change keyup paste', function(){
    if ($('#client-phone').val().length > 12) {
        $('#client-phone').val($('#client-phone').val().substr(0,12));
    }
    });
    
    /*truncando rut*/
    $('input#client-document').live('change keyup paste', function(){
    if ($('input#client-document').val().length > 8) {
        $('input#client-document').val($('input#client-document').val().substr(0,8));
    }
    });