function quitarComunas(arrComunas) {
    let comunas = document.querySelectorAll('#ship-neighborhood option')
    comunas.forEach(comuna => {
        if (arrComunas.includes(comuna.textContent)) {
            comuna.style.display = "none"
        }
    })
}

function comunasOut() {
    setInterval(function() {
      	/*agregar al array futuras comunas a remover*/
        var comunas = [
            'Lirquen', 'Dichato' /* Quita comunas Lirquen y Dichato del listado */
        ];
        quitarComunas(comunas)
        $('#ship-state').bind('change', function() {
            setTimeout(function() {
                quitarComunas(comunas)
            }, 500);
        });
    }, 1500);
}

