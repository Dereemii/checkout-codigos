
function changeAddres(){
    $('#ship-complement').live('change keyup paste', function(){
        $('#ship-complement').attr('maxLength','10');
        $('#ship-complement').attr('oninput','if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);');
           
    });
    $('#ship-number').live('change keyup paste', function(){
        $('#ship-number').prop("type", "number")
        $('#ship-number').prop("pattern", "[0-9]")
        $('#ship-number').prop("style", "border: 1px solid #ccc;border-radius: 2px;box-shadow: none;color: #000;font-family: centrale_sansregular, sans-serif;font-size: 14px;height: 36px;line-height: 33px;padding: 0 10px;")
        $('#ship-number').attr('maxLength','6');
        $('#ship-number').attr('oninput','if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);');
    });
    $('#ship-street').live('change keyup paste', function(){
          $('#ship-street').attr('maxLength','30');
          $('#ship-street').attr('oninput','if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);');
           
       });
        $('#ship-receiverName').live('change keyup paste', function(){
          $('#ship-receiverName').attr('maxLength','20');
          $('#ship-receiverName').attr('oninput','if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);');
           
       });
}




setInterval(function () {
            changeAddres();
        }, 1500);