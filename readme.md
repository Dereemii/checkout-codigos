# Repositorio códigos del checkout

En el siguiente repositorio podrás encontrar los distintos códigos que se han generado en los sitios e-commerce de ECOMSUR para el checkout (Carrito de compras), por todo el equipo de **"Fronts Continuidad"**.

  - Limitaciones de caracteres en el checkout.
  - Validación de formulario, nombre, apellido.
  - Concatena nombre de la región en lista de regiones (Chile).
  - Función para eliminar comunas.

# Sitios revisados

  - Bath&BodyWorks [Link](https://www.bathandbodyworks.cl/)
  - Colgram (Colloky, Opaline) [Link](https://www.colloky.com.pe/)
  - Lee - Wrangler [Link](https://www.leejeans.cl/) [Link](https://www.wrangler.cl/)
  - Tienda Philips [Link](https://www.tienda.philips.cl/)
  - Ficcus [Link](https://www.ficcus.cl/)
  - The North Face [Link](https://www.thenorthface.com.mx/)




  #### *Revisión por célula MJP 06-01-2020*